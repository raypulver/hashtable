#include <stdlib.h>
#include <string.h>
#include "linked.h"

linked_t *linked_new() {
  linked_t *retval = malloc(sizeof(linked_t));
  memset(retval, 0, sizeof(linked_t));
  return retval;
}

void linked_free(linked_t *linked) {
  linked_t *next;
  while (linked) {
    next = linked->tail;
    free(linked);
    linked = next;
  }
}

void linked_assoc(linked_t *list, void *data) {
  list->head = data;
}

void linked_link(linked_t *head, linked_t *tail) {
  head->tail = tail;
}

uint8_t linked_foreach(linked_t *list, uint8_t (apply) (void *, size_t, linked_t *, void *), void *cl) {
  for (size_t i = 0; list; ++i) {
    if (apply(list->head, i, list, cl)) return 0;
    list = list->tail;
  }
  return 1;
} 
