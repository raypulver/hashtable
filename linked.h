#ifndef LINKED_H
#define LINKED_H

typedef struct _linked_t {
  void *head;
  struct _linked_t *tail;
} linked_t;

void linked_assoc(linked_t *, void *);
void linked_link(linked_t *, linked_t *);
linked_t *linked_new();
void linked_free(linked_t *);
uint8_t linked_foreach(linked_t *, uint8_t (void *, size_t, linked_t *, void *), void *);

#endif
