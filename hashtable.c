#include <stdlib.h>
#include <string.h>
#include "hashtable.h"
#include "linked.h"

bucket_t *bucket_new(void *data, size_t data_sz, void *key, size_t key_sz) {
  bucket_t *retval = malloc(sizeof(bucket_t));
  if (!retval) return NULL;
  retval->data = data;
  retval->data_sz = data_sz;
  retval->key = key;
  retval->key_sz = key_sz;
  return retval;
}

void bucket_free(bucket_t *bucket) {
  free(bucket);
}

static uint32_t uint32_t_min(uint32_t a, uint32_t b) {
  return a < b ? a : b;
}

uint32_t hashitem(void *data, size_t sz) {
  uint32_t retval = 0;
  uint8_t *ptr = data;
  void *buffer_ptr = NULL;
  if (sz <= 8) {
    memcpy(((uint8_t *) buffer_ptr) + (8 - sz), data, sz);
    return (uint32_t) ((size_t) buffer_ptr & 0xFFFFFFFF);
  }
  uint32_t buffer = 0;
  size_t idx = 0;
  while (idx < sz) {
    memset(&buffer, 0, sizeof(buffer));
    memcpy(&buffer, ((uint8_t *) data) + idx, uint32_t_min((uint32_t) sizeof(uint32_t), (uint32_t) (sz - idx)));
    retval ^= buffer;
  }
  return retval;
}

hashtable_t *hashtable_new(size_t hint) {
  hashtable_t *retval = malloc(sizeof(hashtable_t));
  retval->mod = hint;
  retval->buckets = calloc(hint, sizeof(linked_t *));
  memset(retval->buckets, 0, sizeof(linked_t *)*hint);
  return retval;
}

void hashtable_free(hashtable_t *ht) {
  for (size_t i = 0; i < ht->mod; ++i) {
    if (ht->buckets[i]) {
      linked_free(ht->buckets[i]);
    }
  }
  free(ht->buckets);
  free(ht);
}

static uint8_t _put_item(void *item_ptr, size_t i, linked_t *list, void *cl) {
  hashtable_put_item_t *packed = cl;
  bucket_t *item = item_ptr;
  if (packed->key_sz != item->key_sz || memcmp(packed->key, item->key, item->key_sz)) return 0;
  item->data = packed->data;
  item->data_sz = packed->data_sz;
  return 1;
}

static uint8_t _get_item(void *item_ptr, size_t i, linked_t *list, void *cl) {
  hashtable_put_item_t *packed = cl;
  bucket_t *item = item_ptr;
  if (packed->key_sz != item->key_sz || memcmp(packed->key, item->key, item->key_sz)) return 0;
  *packed->output = item->data;
  return 1;
}

void hashtable_put(hashtable_t *ht, void *key, size_t key_sz, void *data, size_t data_sz) {
  uint32_t idx = hashitem(key, key_sz) % ht->mod;
  linked_t *b = ht->buckets[idx];
  if (b) {
    hashtable_put_item_t context;
    context.key = key;
    context.key_sz = key_sz;
    context.data = data;
    context.data_sz = data_sz;
    bucket_t *output;
    context.output = (void **) &output;
    if (linked_foreach(b, &_put_item, &context)) {
      linked_t *new_node = linked_new();
      bucket_t *bucket = bucket_new(key, key_sz, data, data_sz);
      new_node->head = bucket;
      new_node->tail = b;
      ht->buckets[idx] = new_node;
    }
    return;
  }
  bucket_t *new_bucket = bucket_new(key, key_sz, data, data_sz);
  linked_t *new_list = linked_new();
  new_list->head = new_bucket;
  ht->buckets[idx] = new_list;
} 

void *hashtable_get(hashtable_t *ht, void *key, size_t key_sz) {
  uint32_t idx = hashitem(key, key_sz);
  linked_t *bucket = ht->buckets[idx];
  if (!bucket) return NULL;
  hashtable_put_item_t context;
  context.key = key;
  context.key_sz = key_sz;
  void *output = NULL;
  context.output = &output;
  if (linked_foreach(bucket, &_get_item, &context)) return NULL;
  return output;
}
