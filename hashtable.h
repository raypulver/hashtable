#ifndef HASHTABLE_H
#define HASHTABLE_H

#include <stddef.h>
#include "linked.h"

typedef struct _bucket_t {
  void *data;
  size_t data_sz;
  void *key;
  size_t key_sz;
} bucket_t;

typedef struct _hashtable_t {
  linked_t **buckets;
  size_t mod;
} hashtable_t;

typedef struct _hashtable_put_item_t {
  void *data;
  size_t data_sz;
  void *key;
  size_t key_sz;
  void **output;
} hashtable_put_item_t;

bucket_t *bucket_new(void *, size_t, void *, size_t);
void bucket_free(bucket_t *);
#endif
